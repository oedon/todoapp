package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

// Delete json item via map and write it all back 
func deleteMapItem() {
	
    a := make(map[int]todoData)

	read := jsonRead()

	for k, v := range read {
		a[k] = todoData{
			Title:    v.Title,
			Deadline: v.Deadline,
			Schedule: v.Schedule,
			Content:  v.Content,
		}
	}

	for k := range read {
		fmt.Println(k, a[k])
	}
	fmt.Println()

    var input int
    fmt.Scan(&input)

	delete(a, input)

	filename := "myFile.json"
	var data []todoData
	for _, v := range a {
		tmp := todoData{
			Title:    v.Title,
			Deadline: v.Deadline,
			Schedule: v.Schedule,
			Content:  v.Content,
		}
		data = append(data, tmp)
	}
	dataBytes, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(data)

}

func addMapItem()  {
    // a := make(map[int]todoData)
    
}
