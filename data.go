package main

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"log"
	"os"
    "fmt"
)

type todoData struct {
	Title    string `json:"Title"`
	Deadline string `json:"Deadline"`
	Schedule string `json:"Schedule"`
	Content  string `json:"Content"`
}

func jsonRead() []todoData {
	filename := "myFile.json"
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	var jsonCall []todoData
	err = json.Unmarshal(data, &jsonCall)
	if err != nil {
		log.Fatal(err)
	}
	return jsonCall
}

func (t *todoData) jsonWrite() {
	filename := "myFile.json"
	err := checkFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		// CLEAN
		log.Fatalf("1 %v", err)
	}
	var data []todoData
	err = json.Unmarshal(file, &data)
	if err != nil {
		// CLEAN
		log.Fatalf("2 %v", err)
	}

	data = append(data, *t)
	dataBytes, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}

}

// Prob DEAD
func (t *todoData) jsonUpdate() {
	filename := "myFile.json"
	var data []todoData

	data = append(data, *t)
	dataBytes, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, dataBytes, 0644)
	if err != nil {
		log.Fatal(err)
	}

}

func jsonNew() {
    var title, deadline, schedule, content string

    fmt.Println("Title: ")
    fmt.Scan(&title)
    fmt.Println("Deadline: ")
    fmt.Scan(&deadline)
    fmt.Println("Schedule: ")
    fmt.Scan(&schedule)
    fmt.Println("Content: ")
    fmt.Scan(&content)

    tmp := todoData{
        Title: title,
        Deadline: deadline,
        Schedule: schedule,
        Content: content,
    }
    tmp.jsonWrite()
}

func checkFile(filename string) error {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		_, err := os.Create(filename)
		if err != nil {
			return err
		}
	}
	return nil
}
