package main

import "fmt"

func menu()  {
	
    for {
		var sel string
		fmt.Println("TODO APP")
		fmt.Println("--------")
		fmt.Println("Select:")
		fmt.Println("New (n)")
		fmt.Println("List (l)")
		fmt.Println("Delete (d)")
		fmt.Println("Quit (q)")
		fmt.Scan(&sel)

		if sel == "n" {
			jsonNew()
		}
		if sel == "l" {
			tmp := jsonRead()
			for k, v := range tmp {
				fmt.Printf("%d %v\n", k, v.Title)
				fmt.Printf("\t %v\n", v.Deadline)
				fmt.Printf("\t %v\n", v.Schedule)
				fmt.Printf("\t %v\n", v.Content)
			}
		}
		if sel == "d" {
			deleteMapItem()
		}
		if sel == "q" {
			break
		}
	}
    
}
